﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walls_Controller : MonoBehaviour
{
    [SerializeField] Obstacle_Holder _obstacleHolder;

    public void Walls_StopAll()
    {
        List<Wall_Movement> Objects = _obstacleHolder.GetList();

        foreach (Wall_Movement wall in Objects)
        {
            wall.StopMovement();
        }
    }
}

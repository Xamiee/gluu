﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay_Controller : MonoBehaviour
{

    [SerializeField] Obstacle_Holder _obstaclesHolder;
    [SerializeField] Event_onScore _eventOnScore;
    [SerializeField] Player_Flip _player;

    List<Wall_Movement> objects;

    void StartScoring()
    {
        _eventOnScore.StartScoring();
    }
    void StopScoring()
    {
        _eventOnScore.EndScoring();
    }
    void AddScore()
    {
        _eventOnScore.AddScore();
    }
    private void Start()
    {
        GetList();
    }

    private void GetList()
    {
        objects = _obstaclesHolder.GetList();
    }

    public void EarlyPhase()
    {
        StartCoroutine("Starting_Phase",0);
    }
    private void GameplayPhase()
    {
        StartScoring();
        _player.ChangeMovement(false);
        StartCoroutine(Gameplay_Phase(9,9));
    }
    public void EndingPhase()
    {
        _player.ChangeMovement(true);
        StopScoring();
        StopAllCoroutines();
        StopCoroutine("Starting_Phase");
        StopCoroutine("Gameplay_Phase");
        
        StartCoroutine("Ending_Phase", 0);
    }

    IEnumerator Starting_Phase(int number)
    {
        yield return new WaitForSeconds(0.5f);
        objects[number].Closing();

        if (number <= 7)
            StartCoroutine("Starting_Phase", number + 1);
        else
        {
            GameplayPhase();
        }
    }
    IEnumerator Gameplay_Phase(int current, int last)
    {
        last = current;


        yield return new WaitForSeconds(Settings.speed);
        if (current == 9)
        {
            current = 8;
        }
        else if(current == 0)
        {
            current = 1;
        }
        else
        {
            int r = Random.Range(0, 2);
            if(r==0)
            {
                current = current - 1;
            }
            if(r==1)
            {
                current = current + 1;
            }
        }
        objects[current].Opening();
        objects[last].Closing();


        AddScore();
        StartCoroutine(Gameplay_Phase(current, last));
    }
    IEnumerator Ending_Phase(int number)
    {
        objects[number].StopAll();
        yield return new WaitForSeconds(0.5f);

            if (number <= 8)
                StartCoroutine("Ending_Phase", number + 1);
        
    }

}

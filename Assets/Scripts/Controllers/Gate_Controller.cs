﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate_Controller : MonoBehaviour
{
    [SerializeField] private Transform gateLeft;
    [SerializeField] private Transform gateRight;

    private float doorup = 4f;
    private float doordown = -1f;


    public void Open_Door()
    {
        Open(true, true);
    }

    public void Close_Door()
    {
        CloseDoorRight();
        CloseDoorLeft();
    }

    public void Open(bool left, bool right)
    {
        if(left)
            OpenDoorLeft();

        if (right)
            OpenDoorRight();
    }

    void CloseDoorLeft()
    {
        iTween.MoveTo(gateLeft.gameObject, iTween.Hash("position", new Vector3(gateLeft.position.x, doordown, gateLeft.position.z), "time", 1f, "easetype", iTween.EaseType.easeInOutQuad));
    }
    void CloseDoorRight()
    {
        iTween.MoveTo(gateRight.gameObject, iTween.Hash("position", new Vector3(gateRight.position.x, doordown, gateRight.position.z), "time", 1f, "easetype", iTween.EaseType.easeInOutQuad));
    }
    void OpenDoorLeft()
    {
        iTween.MoveTo(gateLeft.gameObject, iTween.Hash("position", new Vector3(gateLeft.position.x, doorup, gateLeft.position.z), "time", 1f, "easetype", iTween.EaseType.easeInOutQuad));
    }
    void OpenDoorRight()
    {
        iTween.MoveTo(gateRight.gameObject, iTween.Hash("position", new Vector3(gateRight.position.x, doorup, gateRight.position.z), "time", 1f, "easetype", iTween.EaseType.easeInOutQuad));
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score_Controller : MonoBehaviour
{
    [SerializeField] UI_Score _uiScore;
    bool _scoring = false;
    int currentScore = 0;
    private void Start()
    {
        if(PlayerPrefs.GetInt("HighScore")==-1)
        {
            PlayerPrefs.SetInt("HighScore", 0);
        }
    }
    public void StartScoring()
    {
        currentScore = 0;
        _scoring = true;
    }

    public void EndScoring()
    {
        Debug.Log("ENDSCORING");
        if (PlayerPrefs.GetInt("HighScore")<currentScore)
            PlayerPrefs.SetInt("HighScore", currentScore);
      

        _uiScore.UpdateUi(true);
        _scoring = false;
        
    }

    public void AddScore()
    {
        if(_scoring)
        {
            currentScore++;
            _uiScore.UpdateUi(currentScore);
        }
    }
}

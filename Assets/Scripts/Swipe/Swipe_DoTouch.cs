﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipe_DoTouch : MonoBehaviour
{
    private Vector3 _startMousePoint;
    private Vector3 _endMousePoint;
    private Vector3 _total;
    private GameObject line;
    private Swipe_Controller _swipeController;
    private Swipe_Direction _swipeDirection;
    private bool _doMovement = false;
    private Vector3 MovementPosition;
    [SerializeField] GameObject _player;

    public void Activate(Swipe_Controller swipeController, Swipe_Direction swipeDirection)
    {
        
        _swipeDirection = swipeDirection;
        _swipeController = swipeController;
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _startMousePoint = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            _endMousePoint = Input.mousePosition;

            _total = (_endMousePoint - _startMousePoint).normalized;

            _swipeController.GetDragDirection(_total);


            MovementPosition = _player.transform.position + _total;
            Debug.Log(_total + " - Total");
            Debug.Log(MovementPosition + " - Destination");

            _doMovement = true;
        }


    }

    private void FixedUpdate()
    {

        if (_doMovement)
        {
            DoMovement(MovementPosition);

        }
    }

    void DoMovement(Vector3 total)
    {
        if(total.x >0)
        {
            Debug.Log("goingRight");
            // _player.transform.position = Vector3.Lerp(_player.transform.position, total,1f*Time.deltaTime);
            _player.transform.position = Vector3.MoveTowards(_player.transform.position, new Vector3(total.x, _player.transform.position.y, _player.transform.position.z), 0.1f);

            if (_player.transform.position.x == total.x)
            {
                _doMovement = false;
            }
            // _player.transform.position += new Vector3(-1, 0, 0);
        }
    

        if(total.x <0)
        {
            Debug.Log("goingLeft");
            // _player.transform.position = Vector3.Lerp(_player.transform.position, total,1f*Time.deltaTime);
            _player.transform.position = Vector3.MoveTowards(_player.transform.position, new Vector3(total.x, _player.transform.position.y, _player.transform.position.z),0.1f);

            if (_player.transform.position.x == total.x)
            {
                _doMovement = false;
            }
            // _player.transform.position += new Vector3(-1, 0, 0);
            }
    }
}

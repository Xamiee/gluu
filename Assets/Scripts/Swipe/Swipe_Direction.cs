﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Swipe_Direction : MonoBehaviour
{
    // Start is called before the first frame update
    
    private Vector3 _total;
    private float x, y;

    private Swipe_Controller _swipeController;
    private Swipe_Direction _swipeDirection;
    private Swipe_DoTouch _swipeDoTouch;

    public void Activate(Swipe_Controller swipeController,  Swipe_DoTouch swipeDoTouch)
    {
        _swipeController = swipeController;
        _swipeDoTouch = swipeDoTouch;
    }
    private enum DraggedDirection
    {
        Up,
        Down,
        Right,
        Left
    }

    public void GetDragDirection(Vector3 total)
    {
        GetDirection(total);
    }

    private DraggedDirection GetDirection(Vector3 dragVector)
    {
        float positiveX = Mathf.Abs(dragVector.x);
        float positiveY = Mathf.Abs(dragVector.y);
        DraggedDirection draggedDir;
        if (positiveX > positiveY)
        {
            draggedDir = (dragVector.x > 0) ? DraggedDirection.Right : DraggedDirection.Left;
        }
        else
        {
            draggedDir = (dragVector.y > 0) ? DraggedDirection.Up : DraggedDirection.Down;
        }
        Debug.Log(draggedDir);
        return draggedDir;
    }
}


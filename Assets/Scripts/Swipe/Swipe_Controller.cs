﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipe_Controller : MonoBehaviour
{
    private Vector3 _total;
    [SerializeField] private Swipe_Controller _swipeController;
    [SerializeField] private Swipe_Direction _swipeDirection;
    [SerializeField] private Swipe_DoTouch _swipeDoTouch;

    private void Start()
    {
        _swipeDirection.Activate(_swipeController, _swipeDoTouch);
        _swipeDoTouch.Activate(_swipeController, _swipeDirection);
    }




    //Functions
    public void GetDragDirection(Vector3 total)
    {
        _swipeDirection.GetDragDirection(total);
    }
}

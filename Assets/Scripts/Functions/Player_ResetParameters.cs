﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_ResetParameters : MonoBehaviour
{
    private void OnEnable()
    {
        transform.rotation = new Quaternion(0, 0, 0, 0);
    }
}

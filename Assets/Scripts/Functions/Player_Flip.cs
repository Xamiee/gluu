﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Flip : MonoBehaviour
{
    [SerializeField] GameObject center;
    [SerializeField] GameObject left;
    [SerializeField] GameObject right;

    private int step = 9;
    private float speed = 0.01f;

    bool input = true;

    bool isSingle = false;


    private void OnEnable()
    {
        input = true;
    }
  
    public void ChangeMovement(bool multi)
    {
        if(multi)
        {
            isSingle = false;
        }
        else
        {
            isSingle = true;
        }
    }
    private void Update()
    {
        if (input)
        {
            bool singleTap = Input.GetMouseButtonDown(0);
            bool holding = Input.GetMouseButton(0);
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                StartCoroutine("MoveLeft");
                input = false;
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                StartCoroutine("MoveRight");
                input = false;
            }

            if (isSingle)
            {
                Moving(singleTap);
            }
            else
            {
                Moving(holding);
            }
        

        }
    }
    void Moving(bool type)
    {

        if (type)
        {

            if (Input.mousePosition.x > (Screen.width / 2))
            {
                if (checkDistance(1))
                {
                    StartCoroutine("MoveRight");
                    input = false;
                }
            }
            if (Input.mousePosition.x < (Screen.width / 2))
            {
                if (checkDistance(-1))
                {

                    StartCoroutine("MoveLeft");
                    input = false;
                }
            }
        }
    }
    bool checkDistance(int side)
    {

        RaycastHit2D hit = Physics2D.Raycast(new Vector2((side*0.51f)+ center.transform.position.x, center.transform.position.y), new Vector2(side,0));

        return true;
        //if (hit.collider != null && hit.collider.gameObject.tag!="trigger")
        //{
           
        //    //If the object hit is less than or equal to 6 units away from this object.
        //    if (hit.distance >=1f || hit.distance <=-1f)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        //else
        //{
        //    return true;
        //}
        
        
    }
    IEnumerator MoveLeft()
    {
        for(int i=0; i<(90/step); i++)
        {
            transform.RotateAround(left.transform.position, Vector3.forward, step);
            yield return new WaitForSeconds(speed);
        }
        center.transform.position = transform.position;
        input = true;
    }
    IEnumerator MoveRight()
    {
        for (int i = 0; i < (90 / step); i++)
        {
            transform.RotateAround(right.transform.position, Vector3.back, step);
            yield return new WaitForSeconds(speed);
        }
        center.transform.position = transform.position;
        input = true;
    }
}

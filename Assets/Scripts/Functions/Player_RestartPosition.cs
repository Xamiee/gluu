﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_RestartPosition : MonoBehaviour
{
    
    [SerializeField] Transform _centerObject;
    [SerializeField] Transform _startPoint;


    public void ReturnToPosition()
    {
        gameObject.SetActive(false);
        _centerObject.gameObject.SetActive(false);
        transform.position = _startPoint.transform.position;
        _centerObject.transform.position = _startPoint.transform.position;
        gameObject.SetActive(true);
        _centerObject.gameObject.SetActive(true);
    }
}

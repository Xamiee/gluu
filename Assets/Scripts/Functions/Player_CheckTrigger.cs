﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_CheckTrigger : MonoBehaviour
{
    [SerializeField] Event_OnHit _eventOnHit;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="obstacle")
        {
            _eventOnHit.EventHit();
        }
    }
}

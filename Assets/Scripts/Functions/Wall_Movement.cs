﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall_Movement : MonoBehaviour
{
    bool running = false;
    private float _closeYposition = 1.42f;
    private float _openYposition = 8.92f;


    public void StopMovement()
    {
        iTween.Stop(this.gameObject);
    }
    public void StopAll()
    {
        Open();
    }

    public void Opening()
    {
        Open();
    }
    public void Closing()
    {
        Close();
    }
    void Close()
    {

        iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(gameObject.transform.position.x, _closeYposition, gameObject.transform.position.z), "time", Settings.speed, "easetype", iTween.EaseType.easeInOutQuad, "oncomplete", "RunningOff", "oncompletetarget", this.gameObject));
        // iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 4.5f, gameObject.transform.position.z), "time", 1f, "easetype", iTween.EaseType.easeInOutQuad, "oncomplete", "Back", "oncompletetarget", this.gameObject));
        // iTween.MoveTo(fighter1.gameObject, iTween.Hash("position", new Vector3(fighter1.position.x, fighter1.position.y + 4.5f, fighter1.position.z), "time", 2.5f, "easetype", iTween.EaseType.easeInOutQuad));
        running = true;
    }
    void Open()
    {
        iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(gameObject.transform.position.x, _openYposition, gameObject.transform.position.z), "time", Settings.speed - ((transform.position.y / _openYposition) * Settings.speed), "easetype", iTween.EaseType.easeInOutQuad, "oncomplete", "RunningOff", "oncompletetarget", this.gameObject));
        running = true;
    }

    void RunningOff()
    {
        running = false;
    }
}

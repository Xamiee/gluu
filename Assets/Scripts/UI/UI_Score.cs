﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UI_Score : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI currentScore;
    [SerializeField] TextMeshProUGUI highScore;
    [SerializeField] TextMeshProUGUI gameplay_score;

    private void Start()
    {
        UpdateUi(true);
    }

    public void UpdateUi(int _currentScore)
    {
        currentScore.text = "" + _currentScore;
        gameplay_score.text = "" + _currentScore;
    }
    public void UpdateUi(bool highscore)
    {
        if (PlayerPrefs.GetInt("HighScore") !=-1)
            highScore.text = "" + PlayerPrefs.GetInt("HighScore");
        else
            highScore.text = "NuLL";
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Release_onHit : MonoBehaviour
{

    //Controllers
    
    [SerializeField] Gameplay_Controller _gameplayController;
    [SerializeField] Walls_Controller _wallsController;
    [SerializeField] Gate_Controller _gateController;
    
    //Events
    [SerializeField] Event_onEnterRoom _eventOnEnterRoom;

    //Functions
    [SerializeField] Player_RestartPosition _playerRestartPosition;

    void Start()
    {
        Event_OnHit.onHit += _gateController.Open_Door;
        Event_OnHit.onHit += _wallsController.Walls_StopAll;
        Event_OnHit.onHit += _gameplayController.EndingPhase;
        Event_OnHit.onHit += _eventOnEnterRoom.ResetDoorTrigger;
        Event_OnHit.onHit += _playerRestartPosition.ReturnToPosition;
    }

}

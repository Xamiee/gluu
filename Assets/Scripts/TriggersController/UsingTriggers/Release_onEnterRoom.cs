﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Release_onEnterRoom : MonoBehaviour
{
    [SerializeField] Gate_Controller _gateController;
    [SerializeField] Gameplay_Controller _gameplayController;

    void Start()
    {
        Event_onEnterRoom.onEnterRoom += _gateController.Close_Door;
        Event_onEnterRoom.onEnterRoom += _gameplayController.EarlyPhase;
    }

}

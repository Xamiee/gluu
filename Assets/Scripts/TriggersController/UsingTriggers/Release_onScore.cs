﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Release_onScore : MonoBehaviour
{
    [SerializeField] Score_Controller _scoreController;

    void Start()
    {
        Event_onScore.onScoreStart += _scoreController.StartScoring;
        Event_onScore.onScoreEnd += _scoreController.EndScoring;
        Event_onScore.onScoreAdd += _scoreController.AddScore;
       // Event_onScore.onScoreAdd += _scoreController.ScoreUpdate;
    }

   
    
}

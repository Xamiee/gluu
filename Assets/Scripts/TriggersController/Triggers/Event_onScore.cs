﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_onScore : MonoBehaviour
{
    public delegate void Score();
    public static event Score onScoreStart;
    public static event Score onScoreEnd;
    public static event Score onScoreAdd;


    public void StartScoring()
    {
        onScoreStart();
    }
    public void EndScoring()
    {
        onScoreEnd();
    }
    public void AddScore()
    {
        onScoreAdd();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_onEnterRoom : MonoBehaviour
{
    //#region Singleton
    //private static Event_onEnterRoom _instance;
    //public static Event_onEnterRoom instance;
    //#endregion
    
    public delegate void EnterRoom();
    public static event EnterRoom onEnterRoom;


    bool canOpen = true;


    public void ResetDoorTrigger()
    {

        canOpen = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (canOpen)
        {
            onEnterRoom();
        }

        canOpen = false;
    }
}

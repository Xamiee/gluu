﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_OnHit : MonoBehaviour
{
    

    public delegate void GameOver();
    public static event GameOver onHit;
   

    public void EventHit()
    {
        onHit();
    }
}

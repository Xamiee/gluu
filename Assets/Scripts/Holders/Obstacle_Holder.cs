﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle_Holder : MonoBehaviour
{

    [SerializeField] Wall_Movement fighter1;
    [SerializeField] Wall_Movement fighter2;
    [SerializeField] Wall_Movement fighter3;
    [SerializeField] Wall_Movement fighter4;
    [SerializeField] Wall_Movement fighter5;
    [SerializeField] Wall_Movement fighter6;
    [SerializeField] Wall_Movement fighter7;
    [SerializeField] Wall_Movement fighter8;
    [SerializeField] Wall_Movement fighter9;
    [SerializeField] Wall_Movement fighter10;


    List<Wall_Movement> objects = new List<Wall_Movement>();

    private void Awake()
    {
        objects.Add(fighter1);
        objects.Add(fighter2);
        objects.Add(fighter3);
        objects.Add(fighter4);
        objects.Add(fighter5);
        objects.Add(fighter6);
        objects.Add(fighter7);
        objects.Add(fighter8);
        objects.Add(fighter9);
        objects.Add(fighter10);
    }

        public List<Wall_Movement> GetList()
    {
        return objects;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    #region Singleton
    private static Settings _instance;
    public static Settings instance;
    #endregion

    public static float speed = 0.9f;

    private void Start()
    {
        if(instance==null)
        {
            instance = this;
        }
    }
}

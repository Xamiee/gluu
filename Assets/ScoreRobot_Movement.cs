﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreRobot_Movement : MonoBehaviour
{

    [SerializeField] Transform _player;

    public float smoothSpeed = 10000f;
    public Vector3 offset;

    void LateUpdate()
    {
        Vector3 desiredPosition = new Vector3(_player.position.x, transform.position.y,transform.position.z) + offset;
        // Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = Vector3.Lerp(transform.position, desiredPosition, 0.005f + Time.deltaTime);
        //transform.position = smoothedPosition;

        // transform.LookAt(target);
    }
}
